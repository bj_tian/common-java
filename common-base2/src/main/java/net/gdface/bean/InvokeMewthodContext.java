package net.gdface.bean;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;

/**
 * @author guyadong
 * @since 2.7.0
 */
public class InvokeMewthodContext{
	public final PropertyDescriptor descriptor;
	public final Method method;
	public Object bean; 
	public Object[] values;
	public InvokeMewthodContext(PropertyDescriptor descriptor, Method method, Object bean, Object[] values) {
		this.descriptor = descriptor;
		this.method = method;
		this.bean = bean;
		this.values = values;
	}
}