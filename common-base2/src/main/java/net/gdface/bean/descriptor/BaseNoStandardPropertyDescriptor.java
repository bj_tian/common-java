package net.gdface.bean.descriptor;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;

import net.gdface.bean.InvokeMewthodContext;

/**
 * @author guyadong
 * @since 2.7.0
 */
public class BaseNoStandardPropertyDescriptor extends PropertyDescriptor implements NoStandardPropertyDescriptor {

	public BaseNoStandardPropertyDescriptor(String propertyName, Method readMethod, Method writeMethod)
			throws IntrospectionException {
		super(propertyName, readMethod, writeMethod);
	}
	@Override
	public Method getReadMethod(Class<?> clazz, PropertyDescriptor descriptor) {
		return descriptor.getReadMethod();
	}

	@Override
	public Method getWriteMethod(Class<?> clazz, PropertyDescriptor descriptor) {
		return descriptor.getWriteMethod();
	}

	@Override
	public InvokeMewthodContext beforeInvokeMethod(InvokeMewthodContext context) {
		Object[] args = new Object[context.values.length+2];
    	System.arraycopy(new Object[]{context.bean,context.descriptor.getDisplayName()}, 0, args, 0,2);
    	System.arraycopy(context.values, 0, args, 2, context.values.length);
    	context.bean = null;
    	context.values=args;
		return context;
	}

}
