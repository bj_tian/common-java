package net.gdface.bean.descriptor;

import static net.gdface.utils.CaseSupport.isSnakecase;
import static net.gdface.utils.CaseSupport.toCamelcase;
import static net.gdface.utils.CaseSupport.toSnakecase;

import java.lang.reflect.Field;

import net.gdface.reflection.FieldUtils;

/**
 * @author guyadong
 * @since 2.7.0
 */
public class DescriptorUtils {

	static Field fieldOf(Class<?> beanClass,String name){
		Field field = FieldUtils.getField(beanClass, name,true);
		if(null == field){
			if(isSnakecase(name)){
				field = FieldUtils.getField(beanClass, toCamelcase(name),true);
			}else if(isSnakecase(name)){                        
				field = FieldUtils.getField(beanClass, toSnakecase(name),true);
			}
		}
		return field;
	}

}
