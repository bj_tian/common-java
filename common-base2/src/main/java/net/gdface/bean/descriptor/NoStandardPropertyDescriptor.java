package net.gdface.bean.descriptor;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;

import net.gdface.bean.InvokeMewthodContext;

/**
 * @author guyadong
 * @since 2.7.0
 */
public interface NoStandardPropertyDescriptor {
	public Method getReadMethod(Class<?> clazz, PropertyDescriptor descriptor) ;
	public Method getWriteMethod(Class<?> clazz, PropertyDescriptor descriptor);
	public InvokeMewthodContext beforeInvokeMethod(InvokeMewthodContext context);
}
