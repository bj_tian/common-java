package net.gdface.bean.factory;

import static net.gdface.annotations.AnnotationSupport.createInstanceIfActiveClassPresent;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.google.common.collect.ImmutableList;

import net.gdface.annotations.ActiveOnClass;

/**
 * {@link PropertyDescriptorFactory}对象管理
 * @author guyadong
 * @since 2.7.0
 *
 */
public class PropertyDescriptorFactories implements PropertyDescriptorFactory{
	private final List<PropertyDescriptorFactory> builtinDescriptorFactorys;
	private final List<PropertyDescriptorFactory> nostandardDescriptorFactorys = new ArrayList<>();
	public PropertyDescriptorFactories() {
		builtinDescriptorFactorys  = ImmutableList.<PropertyDescriptorFactory>of(
						new MapDescriptorFactory(),
						new FieldDescriptorFactory()	);
		for(Class<? extends PropertyDescriptorFactory> clazz:Arrays.asList(UnnameRowDescriptorFactory.class,BaseRowDescriptorFactory.class)){
			PropertyDescriptorFactory instance = createInstanceIfActiveClassPresent(clazz);
			if(null != instance){
				nostandardDescriptorFactorys.add(instance);
			}
		}
	}
	public void addFirst(PropertyDescriptorFactory element) {
		if(null != element){
			synchronized (this) {
				builtinDescriptorFactorys.add(0, element);
			}
		}
	}
	public void add(PropertyDescriptorFactory element) {
		if(null != element){
			synchronized (this) {
				builtinDescriptorFactorys.add(element);
			}
		}
	}
	private boolean isActive(ActiveOnClass activeOnClass,Object bean){
		if(null != activeOnClass){			
			for(Class<?> clazz:activeOnClass.value()){
				if(clazz.isInstance(bean)){
					return true;
				}
			}
			for(String clazzName:activeOnClass.name()){
				if(clazzName.equals(bean.getClass().getName())){
					return true;
				}
			}
			return false;
		}
		return true;
	}
	@Override
	public PropertyDescriptor descriptorOf(Object bean, String name) throws IntrospectionException{
		PropertyDescriptor descriptor = nostandardDescriptorOf(bean,name);
		if(null != descriptor){
			return descriptor;
		}
		return builtinDescriptorOf(bean,name);
	}
	/**
	 * 从非标准{@link PropertyDescriptorFactory}中返回{@code bean}中{@code name}指定字段的{@link PropertyDescriptor}实例,没有则返回{@code null}
	 * @param bean
	 * @param name
	 * @throws IntrospectionException
	 * @since 2.7.4
	 */
	public PropertyDescriptor nostandardDescriptorOf(Object bean, String name) throws IntrospectionException{
		PropertyDescriptor descriptor;
		for(PropertyDescriptorFactory factory:nostandardDescriptorFactorys){
			ActiveOnClass activeOnClass = factory.getClass().getAnnotation(ActiveOnClass.class);
			if(isActive(activeOnClass,bean)){
				descriptor =  factory.descriptorOf(bean, name);
				if(null != descriptor){
					return descriptor;
				}
			}
		}
		return null;
	}
	/**
	 * 从内置{@link PropertyDescriptorFactory}中返回{@code bean}中{@code name}指定字段的{@link PropertyDescriptor}实例,没有则返回{@code null}
	 * @param bean
	 * @param name
	 * @throws IntrospectionException
	 * @since 2.7.4
	 */
	public PropertyDescriptor builtinDescriptorOf(Object bean, String name) throws IntrospectionException{
		PropertyDescriptor descriptor;
		for(PropertyDescriptorFactory factory:builtinDescriptorFactorys){
			ActiveOnClass activeOnClass = factory.getClass().getAnnotation(ActiveOnClass.class);
			if(isActive(activeOnClass,bean)){
				descriptor =  factory.descriptorOf(bean, name);
				if(null != descriptor){
					return descriptor;
				}
			}
		}
		return null;
	}
}
