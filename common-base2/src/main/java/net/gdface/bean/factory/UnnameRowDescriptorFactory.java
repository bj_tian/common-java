package net.gdface.bean.factory;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;

import gu.sql2java.RowMetaData;
import gu.sql2java.UnnameRow;
import net.gdface.annotations.ActiveOnClass;
import net.gdface.bean.descriptor.UnnameRowPropertyDescriptor;

/**
 * @author guyadong
 * @since 2.7.0
 *
 */
@ActiveOnClass(UnnameRow.class)
public class UnnameRowDescriptorFactory implements PropertyDescriptorFactory {

	@Override
	public PropertyDescriptor descriptorOf(Object bean, String name) throws IntrospectionException {
		if(bean instanceof UnnameRow ){
			RowMetaData metaData = ((UnnameRow)bean).fetchMetaData();
			if(metaData.columnIDOf(name) >= 0){
				return new UnnameRowPropertyDescriptor(metaData,name);
			}
		}
		return null;
	}

}
