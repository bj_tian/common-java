package net.gdface.bean.factory;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;

import gu.sql2java.BaseRow;
import gu.sql2java.RowMetaData;
import net.gdface.annotations.ActiveOnClass;
import net.gdface.bean.descriptor.BaseRowPropertyDescriptor;

/**
 * @author guyadong
 * @since 2.7.0
 *
 */
@ActiveOnClass(BaseRow.class)
public class BaseRowDescriptorFactory implements PropertyDescriptorFactory {

	@Override
	public PropertyDescriptor descriptorOf(Object bean, String name) throws IntrospectionException {
		if(bean instanceof BaseRow ){
			RowMetaData metaData = ((BaseRow)bean).fetchMetaData();
			if(metaData.columnIDOf(name) >= 0){
				return new BaseRowPropertyDescriptor(name,(BaseRow)bean);
			}
		}
		return null;
	}

}
