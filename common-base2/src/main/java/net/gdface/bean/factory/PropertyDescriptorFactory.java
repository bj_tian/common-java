package net.gdface.bean.factory;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;

/**
 * @author guyadong
 * @since 2.7.0
 */
public interface PropertyDescriptorFactory {
	/**
	 * 返回{@code bean}中{@code name}指定字段的{@link PropertyDescriptor}实例,没有则返回{@code null}
	 * @param bean
	 * @param name
	 * @throws IntrospectionException
	 */
	public PropertyDescriptor descriptorOf(Object bean,String name) throws IntrospectionException;
}
