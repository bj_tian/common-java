package net.gdface.bean.factory;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;

import net.gdface.bean.descriptor.FieldPropertyDescriptor;

/**
 * @author guyadong
 * @since 2.7.0
 */
public class FieldDescriptorFactory implements PropertyDescriptorFactory {

	@Override
	public PropertyDescriptor descriptorOf(Object bean, String name) throws IntrospectionException {
		return FieldPropertyDescriptor.create(bean, name);
	}

}
