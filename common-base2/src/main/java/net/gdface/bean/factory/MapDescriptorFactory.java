package net.gdface.bean.factory;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.util.Map;

import net.gdface.annotations.ActiveOnClass;
import net.gdface.bean.descriptor.MapPropertyDescriptor;

/**
 * @author guyadong
 * @since 2.7.0
 */
@ActiveOnClass(Map.class)
public class MapDescriptorFactory implements PropertyDescriptorFactory {

	@Override
	public PropertyDescriptor descriptorOf(Object bean, String name) throws IntrospectionException {
		if(bean instanceof Map){
    		return new MapPropertyDescriptor(null,name);
    	}
		return null;
	}

}
