package net.gdface.bean.exception;

/**
 * @author guyadong
 * @since 2.7.0
 *
 */
public class BeanPropertyRuntimeException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public BeanPropertyRuntimeException() {
		super();
	}

	public BeanPropertyRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	public BeanPropertyRuntimeException(String message) {
		super(message);
	}

	public BeanPropertyRuntimeException(Throwable cause) {
		super(cause);
	}

}
