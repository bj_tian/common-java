package net.gdface.bean.exception;

/**
 * 循环引用异常
 * @author guyadong
 * @since 2.7.6
 */
public class CycleReferenceException extends BeanPropertyRuntimeException {

	private String nestedName;
	private String value;
	private static final long serialVersionUID = 3487100098252811608L;

	public CycleReferenceException(String nestedName, String value) {
		super(String.format("child: %s:%s", nestedName,value));
		this.nestedName = nestedName;
		this.value = value;
	}

	public String getNestedName() {
		return nestedName;
	}

	public void setNestedName(String nestedName) {
		this.nestedName = nestedName;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
