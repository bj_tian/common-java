package net.gdface.bean.exception;

/**
 * copy from org.apache.commons.beanutils.NestedNullException<br>
 * Thrown to indicate that the <em>Bean Access Language</em> cannot execute query
 * against given bean since a nested bean referenced is null.
 *
 * @since 2.7.0
 */

public class NestedNullException extends IllegalArgumentException {
	private static final long serialVersionUID = -2486056321484771629L;

    // --------------------------------------------------------- Constuctors


	/**
     * Constructs a <code>NestedNullException</code> without a detail message.
     */
    public NestedNullException() {
        super();
    }

    /**
     * Constructs a <code>NestedNullException</code> without a detail message.
     *
     * @param message the detail message explaining this exception
     */
    public NestedNullException(String message) {
        super(message);
    }

	public NestedNullException(String message, Throwable cause) {
		super(message, cause);
		// TODO 自动生成的构造函数存根
	}
}
