package net.gdface.bean;

import java.lang.reflect.InvocationTargetException;

import net.gdface.bean.BeanPropertySupport.NestedContext;

/**
 * @author guyadong
 * @since 2.7.3
 */
public interface NestedNodeVisitor {
	/**
	 * 访问节点前调用
	 * @param context
	 * @return 返回{@code true}时结束调用返回{@link NestedContext#bean}
	 */
	public boolean beforeVisit(NestedContext context);
	/**
	 * 访问子节点前调用
	 * @param context
	 * @return 返回{@code true}时结束调用返回{@link NestedContext#bean}
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	public boolean beofreNext(NestedContext context) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException;
	/**
	 * 访问子节点后调用
	 * @param context
	 * @return 返回{@code true}时结束调用返回父节点的{@link NestedContext#bean},否则返回访问子节点的返回值 
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws IllegalAccessException
	 */
	public boolean afterNext(NestedContext context) throws InvocationTargetException, NoSuchMethodException, IllegalAccessException;
	/**
	 * 访问末端节点
	 * @param context
	 * @return 返回{@code true}时返回末端节点的{@link NestedContext#value},否则返回{@code null}
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws IllegalAccessException
	 */
	public boolean lastNode(NestedContext context) throws InvocationTargetException, NoSuchMethodException, IllegalAccessException;
	
}
