package net.gdface.annotations;

import net.gdface.json.JsonSupport;

/**
 * @author guyadong
 * @since 2.7.0
 */
public class AnnotationSupport {
	/**
	 * 创建指定的{@link JsonSupport}子类实例<br>
	 * 如果获取{@link ActiveOnClass}注解失败或注解中name字段指定的类不存在则代表子类依赖的库不存在，返回null
	 * @param clazz
	 */
	public static <T>T createInstanceIfActiveClassPresent(Class<T> clazz){
		try {
			/** 获取注解失败或注解中name字段指定的类不存在则返回null */
			ActiveOnClass annot = clazz.getAnnotation(ActiveOnClass.class);
			if(null != annot){
				for(String className:annot.name()){
					Class.forName(className);
				}
			}
			return clazz.newInstance();
		} catch (Exception e) {
		}
		return null;		
	}
}
