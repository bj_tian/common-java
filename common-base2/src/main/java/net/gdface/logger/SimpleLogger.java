package net.gdface.logger;

import java.util.logging.Logger;
import static net.gdface.logger.SimpleConsoleFormatter.installFormatter;

/**
 * @author guyadong
 * @since 2.7.3
 */
public class SimpleLogger {

	public static Logger getLogger(String name) {
		 return installFormatter(Logger.getLogger(name));
	}
}
