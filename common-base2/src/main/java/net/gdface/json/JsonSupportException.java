package net.gdface.json;

/**
 * 封装JSON序列化/反序列化异常
 * @author guyadong
 * @since 2.7.3
 */
public class JsonSupportException extends RuntimeException {
	private static final long serialVersionUID = 1538596363606465576L;

	public JsonSupportException(String message, Throwable cause) {
		super(message, cause);
	}

	public JsonSupportException(String message) {
		super(message);
	}

	public JsonSupportException(Throwable cause) {
		super(cause);
	}

}
