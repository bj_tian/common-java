package net.gdface.json;

/**
 * JSON序列化/反序列化支持抽象类
 * @author guyadong
 * @since 2.7.0
 */
public abstract class JsonSupport {
	/**
	 *  对象序列化为JSON字符串[无缩进]
	 * @param object
	 */
	public String toJSONString(Object object){
		return toJSONString(object,false);
	}
	/**
	 * 对象序列化为JSON字符串
	 * @param object
	 * @param prettyFormat 为{@code true}输出缩进的格式化字符串
	 * @throws JsonSupportException JSON序列化异常
	 */
	public abstract String toJSONString(Object object, boolean prettyFormat) throws JsonSupportException;
	/**
	 * 解决JSON字符串为JSON对象，字符串为空或{@code null}则返回空对象(Map)
	 * @param json
	 * @param throwOnfail 为{@code true}时解析抛出异常,否则解析失败则返回原始JSON字符串
	 * @throws JsonSupportException SON序列化异常,{@code throwOnfail}为{@code true}时抛出
	 * @since 2.7.2
	 */
	public Object parseOrEmptyMap(String json,boolean throwOnfail) throws JsonSupportException{
		try {
			return parseOrEmptyMap(json);
		} catch (RuntimeException e) {
			if(!throwOnfail){
				// 解析失败则忽略返回原始值
				return json;
			}
			throw e;
		}
	}
	/**
	 * 解决JSON字符串为JSON对象，字符串为空或{@code null}则返回空对象(Map)
	 * @param json
	 * @throws JsonSupportException JSON解析异常
	 */
	public abstract Object parseOrEmptyMap(String json) throws JsonSupportException;
	/**
	 * 解析JSON字符串为指定的类型的对象，字符串为空或{@code null}则返回{@code null}
	 * @param json
	 * @param targetType
	 * @throws JsonSupportException JSON解析异常
	 */
	public abstract <T>T parse(String json,Class<T> targetType) throws JsonSupportException;
}
