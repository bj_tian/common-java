package net.gdface.json;

import static net.gdface.annotations.AnnotationSupport.createInstanceIfActiveClassPresent;

import java.util.Arrays;

/**
 * @author guyadong
 * @since 2.7.0
 */
public class JsonSupports {
	private static volatile JsonSupport instance;
	/**
	 * 返回 {@link JsonSupport}实例，失败抛出{@link 获取 {@link JsonSupport}实例}
	 */
	public static JsonSupport jsonSupportInstance(){
		if(null == instance){
			synchronized (JsonSupports.class) {
				if(null == instance){
					/** 按顺序尝试创建 fastjson,jackson支持的  JsonSupport 实例 */
					Arrays.asList(FastJsonSupport.class,JacksonSupport.class);
					for(Class<? extends JsonSupport> clazz:Arrays.asList(FastJsonSupport.class,JacksonSupport.class)){
						if(null != (instance = createInstanceIfActiveClassPresent(clazz))){
							break;
						}
					}
				}
				
			}
		}
		if(null == instance){
			throw new IllegalStateException("\nNOT FOUND JsonSupport instance,\n"
					+ "DEFAULT SUPPORT fastjson,jackson,\n"
					+ "You need add dependency com.alibaba:fastjson:1.2.60 OR com.fasterxml.jackson.core:jackson-databind:2.12.5,\n"
					+ "OR call JsonSupport.setInstance to set you custom instance");
		}
		return instance;		
	}
	/**
	 * 设置自定义的{@link JsonSupport}实例
	 * @param instance
	 */
	public static void setJsonSupportInstance(JsonSupport instance) {
		JsonSupports.instance = instance;
	}
}
