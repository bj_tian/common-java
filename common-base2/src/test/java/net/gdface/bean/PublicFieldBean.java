package net.gdface.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class PublicFieldBean {
	public String name;
	public String location;
	public int     groupId;
	public Date createTime = new Date();
	public StandardBean child;
	private Map<String,Object> props;
	@SuppressWarnings("rawtypes")
	private List logs;
	private String jsonProps;
	public String getJsonProps() {
		return jsonProps;
	}
	public void setJsonProps(String jsonProps) {
		this.jsonProps = jsonProps;
	}
	public PublicFieldBean() {
		super();
	}
	@SuppressWarnings("unchecked")
	public PublicFieldBean(String name, String location, int groupId, Date createTime,StandardBean child) {
		super();
		this.name = name;
		this.location = location;
		this.groupId = groupId;
		this.createTime = null == createTime ? new Date() : createTime;
		this.child = child;
		this.logs = new ArrayList<>();
		logs.add("nothing");
	}
	/**
	 * @return props
	 */
	public Map<String, Object> getProps() {
		return props;
	}
	/**
	 * @param props 要设置的 props
	 */
	public void setProps(Map<String, Object> props) {
		this.props = props;
	}
	@SuppressWarnings("rawtypes")
	public List getLogs() {
		return logs;
	}
	@SuppressWarnings("rawtypes")
	public void setLogs(List logs) {
		this.logs = logs;
	}
}
