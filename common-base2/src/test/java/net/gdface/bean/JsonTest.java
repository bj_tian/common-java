package net.gdface.bean;

import org.junit.Test;

import net.gdface.json.FastJsonSupport;
import net.gdface.json.JacksonSupport;

import static net.gdface.utils.SimpleLog.log;
public class JsonTest {

	@Test
	public void testFastjson() {
		FastJsonSupport jsonSupport = new FastJsonSupport();
		log("{}",jsonSupport.toJSONString(null));
		log("{}",jsonSupport.parseOrEmptyMap(null));
		log("{}",jsonSupport.parseOrEmptyMap("{}"));
		
	}
	@Test
	public void test2Jackson() {
		String json = "{\"modified\":0,\"initialized\":8388607,\"new\":false,\"id\":3,\"groupId\":2,\"features\":0,\"name\":\"hello5\",\"physicalAddress\":\"000000000002\",\"addressType\":\"MAC\",\"iotCard\":null,\"status\":\"ENABLE\",\"tokenTime\":0,\"screenInfo\":\"21V960x1080\",\"fixedMode\":\"FLOOR\",\"osArch\":null,\"network\":\"4G\",\"versionInfo\":null,\"model\":\"EAMDEV0\",\"vendor\":null,\"deviceDetail\":{\"device_name\":\"AN01\",\"manufacturer\":\"NXP\",\"made_date\":\"2022-01-02\"},\"props\":{\"disk_capacity\":\"1.2GB\"},\"planId\":\"3709047235ABCEDFG\",\"targetId\":\"20220825182312501665d\",\"remark\":null,\"updateTime\":\"2022-09-01 17:49:22\",\"createTime\":\"2022-08-03 12:21:38\"}";
		JacksonSupport jsonSupport = new JacksonSupport();
		log("{}",jsonSupport.toJSONString(null));
		log("{}",jsonSupport.parseOrEmptyMap(json));
		log("{}",jsonSupport.parseOrEmptyMap("{}"));
		log("{}",int[].class.getComponentType().getSimpleName());
	}

}
