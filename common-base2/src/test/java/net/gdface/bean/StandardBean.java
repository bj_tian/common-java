package net.gdface.bean;

import com.alibaba.fastjson.JSONObject;

public class StandardBean {
	private String historyNumer;
	private String address;
	private JSONObject props = new JSONObject();
	public StandardBean(String historyNumer, String address) {
		super();
		this.historyNumer = historyNumer;
		this.address = address;
	}
	public StandardBean() {
		super();
	}
	public String getHistoryNumer() {
		return historyNumer;
	}
	public void setHistoryNumer(String historyNumer) {
		this.historyNumer = historyNumer;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public JSONObject getProps() {
		return props;
	}
	public StandardBean setProps(JSONObject props) {
		this.props = props;
		return this;
	}
}
