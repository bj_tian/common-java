package net.gdface.json;

import static org.junit.Assert.*;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.collect.ImmutableMap;

import gu.sql2java.json.RawJsonDeserializer;
import net.gdface.utils.SimpleLog;

public class JacksonTest {

	@Test
	public void test1ObjectWriter() {
		ObjectMapper objectMapper = new ObjectMapper();
		JsonTestUser user = new JsonTestUser();
		user.setId(1);
		//user.setName("测试");
		user.setTestPwd("123456");
		user.setTime(new Date());
		user.setFullName(new Name("grace", "brown"));
		JSONArray childs = new JSONArray();
		user.childs(childs);
		childs.fluentAdd("hello").add(1234);
		user.setJson("{\"school\":\"MIT\",\"year\":4}");
		try {
			/** 忽略null值 */
			ObjectWriter writer = objectMapper.writer(SerializationFeature.WRITE_NULL_MAP_VALUES,SerializationFeature.INDENT_OUTPUT);
			String json =writer.writeValueAsString(user);
			SimpleLog.log(json);
			JsonTestUser parsed = objectMapper.readValue(json, JsonTestUser.class);
			SimpleLog.log("parsed {}",parsed);
			assertTrue(user.getJson().equals(parsed.getJson()));
		} catch (IOException e) {
			e.printStackTrace();
			assertFalse(true);
		}
	}
	@Test
	public void testJsonGenerator() {
		ObjectMapper objectMapper = new ObjectMapper();
		JsonTestUser user = new JsonTestUser();
		user.setId(1);
		user.setName("测试");
		user.setTestPwd("123456");
		user.setTime(new Date());
		user.setFullName(new Name("grace", "brown"));
		JSONArray childs = new JSONArray();
		user.childs(childs);
		childs.fluentAdd("hello").add(1234);
		user.setJson("{\"school\":\"MIT\",\"year\":4}");
		try {
			JsonGenerator jsonGenerator = objectMapper.getFactory().createGenerator(System.out, JsonEncoding.UTF8);
			jsonGenerator.writeObject(user);
			System.out.println();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			String json = objectMapper.writeValueAsString(user);
			SimpleLog.log(json);
			JsonTestUser parsed = objectMapper.readValue(json, JsonTestUser.class);
			SimpleLog.log("parsed {}",parsed);
			assertTrue(user.getJson().equals(parsed.getJson()));
		} catch (IOException e) {
			e.printStackTrace();
			assertFalse(true);
		}
	}
	@Test
	public void test2JSONObject(){
		JSONObject jsonobject = new JSONObject();
		jsonobject.fluentPut("name", "world");
		jsonobject.fluentPut("id", 77777);
		jsonobject.fluentPut("date", new Date());		
		jsonobject.fluentPut("childs",new JSONArray().fluentAdd("sim").fluentAdd(ImmutableMap.of("ibm", new Date(),"facebook",new Date())));
		ObjectMapper objectMapper = new ObjectMapper();
	    try {
	      String json = objectMapper.writeValueAsString(jsonobject);
	      System.out.printf(json);
	    } catch (JsonProcessingException e) {
	      e.printStackTrace();
	    }
	}
	@Test
	public void test3ReadTree(){
		String json = "{\"payload\":{\"foo\":\"bar\",\"biz\":[\"lorem\","
	            + "\"ipsum\"]},\"signature\":\"somehmacsign\"}";
		ObjectMapper mapper = new ObjectMapper();
		try {
			JsonNode  node=mapper.readTree(json);
			SimpleLog.log(mapper.writeValueAsString(node));
			SimpleLog.log(node.toString());
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}
	@Test 
	public void test5JsonPropertyAccess(){
		JsonTestUser user = new JsonTestUser();
		try {
			ObjectMapper mapper = new ObjectMapper();
			String json = mapper.writeValueAsString(user);
			SimpleLog.log("json:{}",json);
			assertTrue(!Pattern.compile("\"modified\"").matcher(json).find());
			assertTrue(Pattern.compile("\"createTime\"").matcher(json).find());
			String json2="{\"name\":\"tom\",\"createTime\":\"2002-01-01\",\"modified\":88}";
			//JsonNode node = mapper.readTree(json2);
			JsonTestUser parsed=mapper.readValue(json2, JsonTestUser.class);
			SimpleLog.log("parsed:{}",parsed);
			assertTrue(parsed.getCreateTime().getTime()!=0);
			assertTrue(parsed.getModified()==88);
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}
	@SuppressWarnings("unchecked")
    @Test
	    public void test6ReadAsMap(){
	        String json = "{\"payload\":{\"foo\":\"bar\",\"biz\":[\"lorem\","
	                + "\"ipsum\"]},\"signature\":\"somehmacsign\"}";
	        ObjectMapper mapper = new ObjectMapper();
	        try {
	            Map<String,Object>  map = mapper.readValue(json,Map.class);
	            for(Entry<String, Object> entry:map.entrySet()){
	            	String k=entry.getKey();
	            	Object v=entry.getValue();
	            	SimpleLog.log("{} {} {}",k,v.getClass().getName(),v);
	            }
	        } catch (IOException e) {
	            fail(e.getMessage());
	        }
	    }
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonPropertyOrder(alphabetic=true)
	public static class JsonTestUser{
		/**
		 * id
		 * 不JSON序列化id 
		 */
		@JsonIgnore
		private Integer id;
		/**
		 * 昵称
		 */
		private String name;
		
		@JsonUnwrapped
		private Name fullName;
		/**
		 * 密码
		 * 序列化testPwd属性为pwd
		 */
		@JsonProperty("pwd")
		@JsonRawValue()
		private String testPwd;

		/**
		 * 注册时间
		 * 格式化日期属性  
		 */
		@JsonFormat(pattern = "yyyy-MM-dd")
		private Date time;
		@JsonRawValue()
		@JsonDeserialize(using = RawJsonDeserializer.class)
		private JSON childs = new JSONArray();
		@JsonRawValue
		@JsonDeserialize(using = RawJsonDeserializer.class)
		private String json;
		@JsonProperty(access=Access.WRITE_ONLY)
		private int modified = 0;
		@JsonProperty(access=Access.READ_ONLY)
		@JsonFormat(pattern = "yyyy-MM-dd")
		private Date createTime;
		
		public JsonTestUser() {
			super();
			try {
				createTime = new SimpleDateFormat("yyyy-MM-dd").parse("1973-08-11");
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getTestPwd() {
			return testPwd;
		}

		public Name getFullName() {
			return fullName;
		}

		public void setFullName(Name fullName) {
			this.fullName = fullName;
		}

		public void setTestPwd(String testPwd) {
			this.testPwd = testPwd;
		}

		public Date getTime() {
			return time;
		}

		public void setTime(Date time) {
			this.time = time;
		}
		public String getChilds(){
			return null == childs ? null : childs.toJSONString();
		}
		
		public void setChilds(String childs){
			this.childs =  JSONObject.parseObject(childs,JSONArray.class);
		}
		public JSONArray childs(){
			return (JSONArray) childs;
		}
		public void childs(JSONArray childs){
			this.childs = childs;
		}

		public String getJson() {
			return json;
		}

		public void setJson(String json) {
			this.json = json;
		}

		public int getModified() {
			return modified;
		}

		public void setModified(int modified) {
			this.modified = modified;
		}

		public Date getCreateTime() {
			return createTime;
		}

		public void setCreateTime(Date createTime) {
			this.createTime = createTime;
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("JsonTestUser [");
			if (id != null) {
				builder.append("id=");
				builder.append(id);
				builder.append(", ");
			}
			if (name != null) {
				builder.append("name=");
				builder.append(name);
				builder.append(", ");
			}
			if (fullName != null) {
				builder.append("fullName=");
				builder.append(fullName);
				builder.append(", ");
			}
			if (testPwd != null) {
				builder.append("testPwd=");
				builder.append(testPwd);
				builder.append(", ");
			}
			if (time != null) {
				builder.append("time=");
				builder.append(time);
				builder.append(", ");
			}
			if (childs != null) {
				builder.append("childs=");
				builder.append(childs);
				builder.append(", ");
			}
			if (json != null) {
				builder.append("json=");
				builder.append(json);
				builder.append(", ");
			}
			builder.append("modified=");
			builder.append(modified);
			builder.append(", ");
			if (createTime != null) {
				builder.append("createTime=");
				builder.append(createTime);
			}
			builder.append("]");
			return builder.toString();
		}
	}
	public static class Name {
		 public String first, last;

		public Name(String first, String last) {
			super();
			this.first = first;
			this.last = last;
		}

		public Name() {
			super();
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("Name [");
			if (first != null) {
				builder.append("first=");
				builder.append(first);
				builder.append(", ");
			}
			if (last != null) {
				builder.append("last=");
				builder.append(last);
			}
			builder.append("]");
			return builder.toString();
		}
    }
}
