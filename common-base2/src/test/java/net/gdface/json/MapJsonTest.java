package net.gdface.json;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.google.common.collect.Maps;

import static net.gdface.utils.SimpleLog.log;
import static net.gdface.bean.BeanPropertySupport.BEAN_SUPPORT;

public class MapJsonTest {

	@Test
	public void test1() {
		try {
			//String json = "{\n    \"column\": \"target_id\",\n    \"values\": {\n      \"130\": \"20221214154204568a14e\",\n      \"132\": \"20221214154204568a14e\",\n      \"134\": \"20221214154204568a14e\",\n      \"135\": \"20221214154204568a14e\",\n      \"169\": \"20221214154204568a14e\"\n    },\n    \"tokenId\": \"TOKEN:fffffe7affffffa8\"\n  }";
			String json = "{\"args\":{\"column\":\"target_id\",\"values\":{130:\"20221214154204568a14e\",132:\"20221214154204568a14e\",134:\"20221214154204568a14e\",135:\"20221214154204568a14e\",169:\"20221214154204568a14e\"},\"tokenId\":\"TOKEN:fffffe7affffffa8\"},\"tag_detail\":[{\"id:130\":{\"targetId\":{\"left\":\"202212141421277810bdb\",\"right\":\"20221214154204568a14e\"}}},{\"id:132\":{\"targetId\":{\"left\":\"202212141421277810bdb\",\"right\":\"20221214154204568a14e\"}}},{\"id:134\":{\"targetId\":{\"left\":\"202212141421277810bdb\",\"right\":\"20221214154204568a14e\"}}},{\"id:135\":{\"targetId\":{\"left\":\"202212141421277810bdb\",\"right\":\"20221214154204568a14e\"}}},{\"id:169\":{\"targetId\":{\"left\":\"202212141421277810bdb\",\"right\":\"20221214154204568a14e\"}}}]}";
			JSONObject jsonObject = JSON.parseObject( json,JSONObject.class);
			
			log("jsonObject {}",JSON.toJSONString(jsonObject, true));
			JSONObject args = (JSONObject) jsonObject.get("args");
			log("args:{},keyset:{}",args.getClass(),args.keySet());
			JSONObject values = (JSONObject) args.get("values");
			log("values:{}  {}",values.getClass().getName(),values.keySet());
			log("169:{}",values.get(169));
			log("args.values:{}",BEAN_SUPPORT.getProperty(jsonObject, "args.values.169"));
			ObjectMapper objectMapper = new ObjectMapper();

			log("values:{}",objectMapper.writeValueAsString(args));
		} catch (Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}
	@Test
	public void test2(){
		Map<Integer, Object> map = Maps.newHashMap();
		map.put(12, "hello");
		/** WriteNonStringKeyAsString : 非String类型的Key值都转为String */
		String json = JSON.toJSONString(map);
		log("{}",json);
		JSONObject jsonObject = JSON.parseObject(json);
		log("jsonObject{}",jsonObject);
	} 
	@Test
	public void test3(){
		Map<Boolean, Object> map = Maps.newHashMap();
		map.put(true, "hello");
		String json = JSON.toJSONString(map);
		log("{}",json);
		JSONObject jsonObject = JSON.parseObject(json);
		log("jsonObject{}",jsonObject);
	} 
	class DateAsTimestampSerializer extends JsonSerializer<Integer>
	{
	  @Override
	  public void serialize(Integer value, JsonGenerator jgen, SerializerProvider provider) 
	      throws IOException, JsonProcessingException
	  {
	    jgen.writeFieldName(String.valueOf(value));
	  }
	}
	
}
