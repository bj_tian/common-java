package net.gdface.json;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.Map;

import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.google.common.collect.Maps;

import static net.gdface.utils.SimpleLog.log;
public class FastjsonTest {

	@Test
	public void test() {
		JSON.DEFFAULT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
		log("date {}",JSON.toJSONString(new Date(),SerializerFeature.WriteDateUseDateFormat));
		Map<String,Object> m = Maps.newHashMap();
		m.put("hello",null);
		log("date {}",JSON.toJSONString(m,SerializerFeature.WriteDateUseDateFormat));
	}

}
