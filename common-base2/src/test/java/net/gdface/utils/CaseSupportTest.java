package net.gdface.utils;

import static org.junit.Assert.*;

import org.junit.Test;

import static net.gdface.utils.SimpleLog.log;
import static net.gdface.utils.CaseSupport.*;

public class CaseSupportTest {
	@Test
	public void testCase(){
		
		
		log(toCamelcase("otherProps"));
		log(toSnakecase("OtherPropsP"));
		log(toSnakecase("A1A2"));
		log(toSnakecase("Other Props"));
		log(toSnakecase("otherProps"));
		log(toCamelcase("OTHERPROPS"));
		log(toCamelcase("uer_1"));
		log(toCamelcase("_uer_sam_"));
		log(toCamelcase("_uer_Sam_1"));
		log(toCamelcase("_uer"));
		log(toCamelcase("_uer_S"));
		log(toCamelcase("PerSS"));
		log(toCamelcase("online_status.host_address"));
		assertTrue(CaseSupport.isCamelcase("otherProps"));
		assertFalse(CaseSupport.isCamelcase("   "));
		assertTrue(CaseSupport.isCamelcase("  otherProps "));
		assertTrue(CaseSupport.isCamelcase("  PtherPropsP "));
		assertFalse(CaseSupport.isSnakecase("  PtherPropsP "));
		assertFalse(CaseSupport.isSnakecase("OTHERPROPS"));
		assertTrue(CaseSupport.isSnakecase("_uer_sam_"));
		assertTrue(CaseSupport.isSnakecase("_uer"));
	}
}
