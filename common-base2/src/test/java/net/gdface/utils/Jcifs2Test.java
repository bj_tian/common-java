package net.gdface.utils;

import java.net.UnknownHostException;

import org.junit.Test;

import jcifs.Address;
import jcifs.NameServiceClient;
import jcifs.NetbiosAddress;
import jcifs.context.SingletonContext;

public class Jcifs2Test {

	@Test
	public void test() {
		try {
			NameServiceClient nsc = SingletonContext.getInstance().getNameServiceClient();

			NetbiosAddress[] addrs =	nsc.getNbtAllByAddress("192.168.10.160");
			for(NetbiosAddress address:addrs){
			System.out.printf("%s\n",address.getHostName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Test
	public void test2() {
		try {
			NameServiceClient nsc = SingletonContext.getInstance().getNameServiceClient();
			NetbiosAddress address = nsc.getNbtByName("landfaceloghost");
			System.out.printf("%s\n",address.getHostName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Test
	public void test3() {
		try {
			NameServiceClient nsc = SingletonContext.getInstance().getNameServiceClient();

			{
				// 提供的主机名返回所有绑定的地址对象
				Address[] addrs = nsc.getAllByName("landfaceloghost", true);
				for(Address address : addrs){
					System.out.printf("%s\n",address);
				}
			}
			
			{
				System.out.println("============");
				// 根据提供的主机名解析为Address对象
				Address address = nsc.getByName("landfaceloghost");
				System.out.printf("%s\n",address);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Test
	public void test4(){
		System.out.println(JcifsUtil.changeHostIfLanhost("chenpeng-pc"));
		System.out.println(JcifsUtil.changeHostIfLanhost("www.baidu.com"));
	}
	@Test
	public void test5() {
		try {
			String address = JcifsUtil.hostAddressOf("chenpeng-pc");
			System.out.printf("host ip:%s\n",address);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
}
