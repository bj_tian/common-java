package gu.doc;

import net.gdface.utils.ClassCommentProvider;
import net.gdface.utils.ClassCommentProviderFactory;

public class ClassCommentProviderFactoryImpl implements ClassCommentProviderFactory {

	public ClassCommentProviderFactoryImpl() {
	}

	@Override
	public ClassCommentProvider apply(Class<?> clazz) {
		return new ClassCommentProviderImpl(clazz);
	}

}
