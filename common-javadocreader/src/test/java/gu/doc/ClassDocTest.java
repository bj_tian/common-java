package gu.doc;

import org.junit.Test;

import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.RootDoc;

import gu.doc.ExtClassDoc;
import gu.doc.JavadocReader;

public class ClassDocTest {

	@Test
	public void testReadDocs() {
		RootDoc rootDoc = JavadocReader.readDocs("net.gdface.service.facelog","/home/gyd/workspace.java/facelog/db2/target/classes","/home/gyd/workspace.java/facelog/facelog-service/src/main/java");
		for(ClassDoc classDoc:rootDoc.classes()){
			System.out.println(classDoc.qualifiedName());
		}
	}
	@Test
	public void testReadDoc(){

/*		com.sun.tools.javadoc.Main.execute(new String[] {"-doclet", 
				Doclet.class.getName(), 
				"-docletpath", 
				Doclet.class.getResource("/").getPath(),
				"-classpath",
				"D:/j/facelog/facelog-main/target/classes;D:/j/facelog/db/target/classes;D:/j/facelog/db/sql2java/lib/swift-annotations-0.14.2.jar",
				"J:/facelog/facelog-main/src/main/java/net/gdface/facelog/service/BaseFaceLog.java"});*/
/*		com.sun.tools.javadoc.Main.execute(JavaDocReader.class.getClassLoader(),new String[] {"-doclet", 
				Doclet.class.getName(), "-quiet","-Xmaxerrs","1","-Xmaxwarns","1",
				"J:/facelog/facelog-main/src/main/java/net/gdface/facelog/service/BaseFaceLog.java"});*/
		ExtClassDoc doc = JavadocReader.read("J:/facelog/facelog-base/src/main/java/net/gdface/facelog/IFaceLog.java");
//		Method method = FaceLogDefinition.class.getMethod("setPersonExpiryDate", int.class,long.class);
//		String methodDoc = doc.getMethodComment(method);
		doc.addExcludeTag("@throws");
		//ExtClassDoc.additionalText("hello,add begin of class", Action.ADD, Type.CLASS);
		doc.additionalText("hello,add END of comment", "OVERWRITE", "CLASS");
//		for(Method method:FaceLogDefinition.class.getDeclaredMethods()){
//			System.out.println(method);
//			MethodDocImpl methodDoc = doc.getMethodDoc(method);
//			if(null != methodDoc){
//				System.out.println("matched:" + methodDoc);
//			}else
//				System.out.println("not matched ");
//		}
		doc.output(System.out);
//		show();
	
	}	
	
	@Test
	public void  TestReadDocsOnLinux() {
		RootDoc rootDoc = JavadocReader.readDocs(
				"net.facelib.authkernel",
				"/home/gyd/workspace.java/authkernel/authkernel-local/target/classes;/home/gyd/workspace.java/authkernel/authkernel-db/target/classes;/ilock/maven_repository/com/gitee/l0km/sql2java-base/2.6.5/sql2java-base-2.6.5.jar",
				"/home/gyd/workspace.java/authkernel/authkernel-local/src/main/java;/home/gyd/workspace.java/authkernel/authkernel-local/src/sql2java/java");
		for(ClassDoc doc:rootDoc.classes()) {
			System.out.printf("doc %s\n",doc);
		}
	}
		

}
