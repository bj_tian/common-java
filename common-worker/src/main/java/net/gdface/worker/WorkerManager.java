package net.gdface.worker;

import java.util.concurrent.TimeUnit;

/**
 * 工作线程管理器接口
 * @author guyadong
 *
 */
public interface WorkerManager {

	/**
	 * 启动工作线程管理器<br>
	 * @return 监视线程
	 */
	public abstract Thread startManager();

	/**
	 * 等待监视线程结束，如果任务队列为空而仍有活动线程，则超时后强制结束
	 * 
	 * @param waitTime 等待的时间
	 * @param unit 时间单位
	 * @throws InterruptedException
	 */
	public abstract void stopManager(long waitTime, TimeUnit unit) throws InterruptedException;

}