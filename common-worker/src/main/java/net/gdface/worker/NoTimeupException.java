/**   
* @Title: NoTimeupException.java 
* @Package net.gdface.service.search 
* @Description: TODO 
* @author guyadong   
* @date 2015年5月7日 上午10:18:48 
* @version V1.0   
*/
package net.gdface.worker;

import java.util.concurrent.TimeUnit;

import net.gdface.utils.Assert;

/**
 * 延时控制异常<br>
 * @author guyadong
 *
 */
public class NoTimeupException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 216927048988094851L;
	/**
	 * 还剩余的时间
	 */
	private long gap=0L;
	/**
	 * 时间单位
	 */
	private TimeUnit unit=TimeUnit.MILLISECONDS;
	/**
	 * 用指定的时间单位返回时间差值
	 * @return gap
	 */
	public long getGap(TimeUnit unit) {
		return unit.convert(this.gap, this.unit);
	}
	/**
	 * 返回时间差值(毫秒)
	 * @return gap
	 */
	public long getGapMills() {
		return TimeUnit.MILLISECONDS.convert(this.gap, this.unit);
	}
	
	/**
	 * 
	 */
	public NoTimeupException() {
	}
	
	/**
	 * @param gapMills 剩余的时间(毫秒)
	 */
	public NoTimeupException(long gapMills) {
		this.gap=gapMills;
	}
	/**
	 * @param gap 剩余的时间
	 * @param unit 时间单位
	 */
	public NoTimeupException(long gap,TimeUnit unit) {
		Assert.notNull(unit, "unit");
		this.gap=gap;
		this.unit=unit;
	}
	/**
	 * @param message
	 */
	public NoTimeupException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public NoTimeupException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public NoTimeupException(String message, Throwable cause) {
		super(message, cause);
	}

}
