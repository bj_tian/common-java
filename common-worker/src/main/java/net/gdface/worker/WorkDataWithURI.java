/**   
* @Title: WorkDataWithURI.java 
* @Package net.gdface.worker 
* @Description: TODO 
* @author guyadong   
* @date 2015年5月17日 下午9:49:59 
* @version V1.0   
*/
package net.gdface.worker;

import java.net.URI;

/**
 * @author guyadong
 *
 */
public abstract class WorkDataWithURI extends WorkData {
	/**
	 * 
	 */
	public WorkDataWithURI() {
	}
	public abstract URI getURI();
}
