package net.gdface.common;

import java.lang.reflect.Array;
import java.util.Iterator;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.TypeHandler;

import com.google.common.base.Joiner;


public class OptionTest {
    private static Object getParsedOptionValue(Option option,String value) throws ParseException {
        
        if (option == null || value == null)
        {
            return null;
        }
        
        return TypeHandler.createValue(value, option.getType());
    }
	public static void main(String[] args) throws Exception {
		CommandLineParser parser = new DefaultParser();

		Options options = new Options();
		
		options.addOption(Option.builder("k").longOpt("key")
				.desc("keys").required().numberOfArgs(Option.UNLIMITED_VALUES).build());
		// 处理Options和参数
		CommandLine cmd = parser.parse(options, args);
		Object value;

		Iterator<Option> it = options.getOptions().iterator();
		
		while (it.hasNext()) {
			Option opt = it.next();
			String key = opt.getLongOpt() == null ? opt.getOpt() : opt.getLongOpt();

			if(opt.hasArg()){
				if(opt.getArgs() == 1){
					value=cmd.getParsedOptionValue(key);
					System.out.printf("%s:%s\n",key,value);
				}else{
					// 当选项允许多个参数时返回T[]
					String[] values = cmd.getOptionValues(key);
					// 根据选项的类型创建数组
					value = Array.newInstance((Class<?>) opt.getType(), values.length);
					for(int i = 0; i < values.length; ++i){							
						Array.set(value, i, getParsedOptionValue(opt,values[i]));
					}
					System.out.printf("%s:%s\n",key,Joiner.on(',').join((Object[])value));						
				}
			}else{
				// 不需要参数的选项视为boolean,直接返回boolean值
				value = cmd.hasOption(key);
				System.out.printf("%s:%s\n",key,value);
			}
			
		}
	}
}
