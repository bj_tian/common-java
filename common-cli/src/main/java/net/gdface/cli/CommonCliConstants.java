package net.gdface.cli;

public interface CommonCliConstants {

	final String HELP_OPTION = "h";
	final String HELP_OPTION_LONG = "help";
	final String HELP_OPTION_DESC = "Print this usage information";
	final String DEFINE_OPTION = "D";
	final String DEFINE_OPTION_DESC = "define value for given property for System.getPropety(String)";
	final String TRACE_OPTION = "X";
	final String TRACE_OPTION_LONG = "trace";
	final String TRACE_OPTION_DESC = "show stack trace on error ,default: false";
}