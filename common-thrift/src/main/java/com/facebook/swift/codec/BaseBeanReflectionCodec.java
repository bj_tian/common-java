package com.facebook.swift.codec;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.concurrent.Immutable;

import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolException;

import com.facebook.swift.codec.internal.TProtocolReader;
import com.facebook.swift.codec.internal.reflection.ReflectionThriftStructCodec;
import com.facebook.swift.codec.metadata.ThriftConstructorInjection;
import com.facebook.swift.codec.metadata.ThriftFieldInjection;
import com.facebook.swift.codec.metadata.ThriftFieldMetadata;
import com.facebook.swift.codec.metadata.ThriftInjection;
import com.facebook.swift.codec.metadata.ThriftMethodInjection;
import com.facebook.swift.codec.metadata.ThriftParameterInjection;
import com.facebook.swift.codec.metadata.ThriftStructMetadata;
import com.google.common.base.Throwables;
import com.google.common.collect.Lists;

import net.gdface.thrift.BaseThriftUtils;
import static com.facebook.swift.codec.metadata.FieldKind.THRIFT_FIELD;
import static java.lang.String.format;
import static com.google.common.base.Preconditions.checkArgument;

/**
 * <a href="https://gitee.com/l0km/sql2java">sql2java</a> gu.sql2java.BaseBean 接口实例的编解码实现<br>
 * 重写{@link ReflectionThriftStructCodec#read(TProtocol)}方法,
 * BaseBean 在所有其他字段被注入后才执行实例的内置字段{@code initialized},{@code modified},
 * 以实现不论{@link ThriftStructMetadata#getMethodInjections()}返回的字段顺序如何,内置字段都在最后注入
 * @author guyadong
 *
 * @param <T>
 */
@Immutable
public class BaseBeanReflectionCodec<T> extends ReflectionThriftStructCodec<T>
{
	public BaseBeanReflectionCodec(ThriftCodecManager manager, ThriftStructMetadata metadata)
	{
		super(manager, metadata);
		try {
			Class<?> baseBeanClass = Class.forName("gu.sql2java.BaseBean",false,this.getClass().getClassLoader());
			checkArgument(baseBeanClass.isAssignableFrom((Class<?>) metadata.getStructType()));
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
    @Override
    public T read(TProtocol protocol)
            throws Exception
    {
        TProtocolReader reader = new TProtocolReader(protocol);
        reader.readStructBegin();

        Map<Short, Object> data = new HashMap<>(metadata.getFields().size());
        while (reader.nextField()) {
            short fieldId = reader.getFieldId();

            // do we have a codec for this field
            ThriftCodec<?> codec = fields.get(fieldId);
            if (codec == null) {
                reader.skipFieldData();
                continue;
            }

            // is this field readable
            ThriftFieldMetadata field = metadata.getField(fieldId);
            if (field.isReadOnly() || field.getType() != THRIFT_FIELD) {
                reader.skipFieldData();
                continue;
            }

            // read the value
            Object value = reader.readField(codec);
            if (value == null) {
              if (field.getRequiredness() == ThriftField.Requiredness.REQUIRED) {
                throw new TProtocolException("required field was not set");
              } else {
                continue;
              }
            }

            data.put(fieldId, value);
        }
        reader.readStructEnd();

        // build the struct
        return constructStruct(data);
    }
    @SuppressWarnings("unchecked")
    private T constructStruct(Map<Short, Object> data)
            throws Exception
    {
        // construct instance
        Object instance;
        {
            ThriftConstructorInjection constructor = metadata.getConstructorInjection().get();
            Object[] parametersValues = new Object[constructor.getParameters().size()];
            for (ThriftParameterInjection parameter : constructor.getParameters()) {
                Object value = data.get(parameter.getId());
                parametersValues[parameter.getParameterIndex()] = value;
            }

            try {
                instance = constructor.getConstructor().newInstance(parametersValues);
            }
            catch (InvocationTargetException e) {
                if (e.getTargetException() != null) {
                    Throwables.throwIfInstanceOf(e.getTargetException(), Exception.class);
                }
                throw e;
            }
        }

        // inject fields
        for (ThriftFieldMetadata fieldMetadata : metadata.getFields(THRIFT_FIELD)) {
            for (ThriftInjection injection : fieldMetadata.getInjections()) {
                if (injection instanceof ThriftFieldInjection) {
                    ThriftFieldInjection fieldInjection = (ThriftFieldInjection) injection;
                    Object value = data.get(fieldInjection.getId());
                    if (value != null) {
                        fieldInjection.getField().set(instance, value);
                    }
                }
            }
        }
        List<ThriftMethodInjection> methodInjections = Lists.newArrayList(metadata.getMethodInjections());
        final List<ThriftMethodInjection> builtinInjections = Lists.newArrayList();
        // 将BaseBean内置字段排除保存到 builtinInjections
        for(Iterator<ThriftMethodInjection> itor = methodInjections.iterator();itor.hasNext();){
        	ThriftMethodInjection input = itor.next();
        	if(BaseThriftUtils.BASEBEAN_BUILTIN_FIELDS.contains(input.getParameters().get(0).getName())){
        		builtinInjections.add(input);
        		itor.remove();
        	}
        }
        
        // inject methods
        injectMethods(data,instance,methodInjections);
        // 所有其他字段都执行完成注入后再执行内置字段的注入
        injectMethods(data,instance,builtinInjections);

        // builder method
        if (metadata.getBuilderMethod().isPresent()) {
            ThriftMethodInjection builderMethod = metadata.getBuilderMethod().get();
            Object[] parametersValues = new Object[builderMethod.getParameters().size()];
            for (ThriftParameterInjection parameter : builderMethod.getParameters()) {
                Object value = data.get(parameter.getId());
                parametersValues[parameter.getParameterIndex()] = value;
            }

            try {
                instance = builderMethod.getMethod().invoke(instance, parametersValues);
                if (instance == null) {
                    throw new IllegalArgumentException("Builder method returned a null instance");

                }
                if (!metadata.getStructClass().isInstance(instance)) {
                    throw new IllegalArgumentException(format("Builder method returned instance of type %s, but an instance of %s is required",
                            instance.getClass().getName(),
                            metadata.getStructClass().getName()));
                }
            }
            catch (InvocationTargetException e) {
                if (e.getTargetException() != null) {
                    Throwables.throwIfInstanceOf(e.getTargetException(), Exception.class);
                }
                throw e;
            }
        }

        return (T) instance;
    }
    
    private static void injectMethods(Map<Short, Object> data,Object instance,Iterable<ThriftMethodInjection> methodInjections) throws Exception{
        for (ThriftMethodInjection methodInjection : methodInjections) {
            boolean shouldInvoke = false;
            Object[] parametersValues = new Object[methodInjection.getParameters().size()];
            for (ThriftParameterInjection parameter : methodInjection.getParameters()) {
                Object value = data.get(parameter.getId());
                if (value != null) {
                    parametersValues[parameter.getParameterIndex()] = value;
                    shouldInvoke = true;
                }
            }

            if (shouldInvoke) {
                try {
                    methodInjection.getMethod().invoke(instance, parametersValues);
                }
                catch (InvocationTargetException e) {
                    if (e.getTargetException() != null) {
                        Throwables.throwIfInstanceOf(e.getTargetException(), Exception.class);
                    }
                    throw e;
                }
            }
        }
    }
}
