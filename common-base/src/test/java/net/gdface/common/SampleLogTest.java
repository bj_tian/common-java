package net.gdface.common;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

import net.gdface.utils.SimpleLog;

public class SampleLogTest {

	@Test
	public void test() {
		SimpleLog.log("wwwwww", "tom");
		SimpleLog.log("name {},age:{}");
		SimpleLog.log("name {},age:{} ww", "tom");
		SimpleLog.log("name {},age:{},date:{},time:{}", "tom",23,new Date());
	}

}
