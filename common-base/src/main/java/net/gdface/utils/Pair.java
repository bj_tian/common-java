package net.gdface.utils;


import java.io.Serializable;

 /**
  * <p>A convenience class to represent name-value pairs.</p>
  * copy from javafx.util.Pair,and refer to org.apache.commons.lang3.tuple.Pair
  */
public class Pair<L,R> implements Serializable{
	private static final long serialVersionUID = -4496481498313465718L;

    /** Left object */
    public final L left;
    /** Right object */
    public final R right;

	//-----------------------------------------------------------------------
	/**
	 * <p>Gets the left element from this pair.</p>
	 * 
	 * <p>When treated as a left-value pair, this is the left.</p>
	 * 
	 * @return the left element, may be null
	 */
	public L getLeft(){ return left; }

	/**
	 * <p>Gets the right element from this pair.</p>
	 * 
	 * <p>When treated as a left-value pair, this is the value.</p>
	 * 
	 * @return the right element, may be null
	 */
	public R getRight(){ return right; }

	/**
     * Gets the left for this pair.
     * @return left for this pair
     */
    public L getKey() { return getLeft(); }

    /**
     * Gets the value for this pair.
     * @return value for this pair
     */
    public R getValue() { return getRight(); }
    
    /**
     * Creates a new pair
     * @param left The left for this pair
     * @param value The value to use for this pair
     */
    public Pair(L key,  R value) {
        this.left = key;
        this.right = value;
    }

    /**
     * <p><code>String</code> representation of this
     * <code>Pair</code>.</p>
     *
     * <p>The default name/value delimiter '=' is always used.</p>
     *
     *  @return <code>String</code> representation of this <code>Pair</code>
     */
    @Override
    public String toString() {
        return left + "=" + right;
    }

    /**
     * <p>Generate a hash code for this <code>Pair</code>.</p>
     *
     * <p>The hash code is calculated using both the name and
     * the value of the <code>Pair</code>.</p>
     *
     * @return hash code for this <code>Pair</code>
     */
    @Override
    public int hashCode() {
        // name's hashCode is multiplied by an arbitrary prime number (13)
        // in order to make sure there is a difference in the hashCode between
        // these two parameters:
        //  name: a  value: aa
        //  name: aa value: a
        return left.hashCode() * 13 + (right == null ? 0 : right.hashCode());
    }

     /**
      * <p>Test this <code>Pair</code> for equality with another
      * <code>Object</code>.</p>
      *
      * <p>If the <code>Object</code> to be tested is not a
      * <code>Pair</code> or is <code>null</code>, then this method
      * returns <code>false</code>.</p>
      *
      * <p>Two <code>Pair</code>s are considered equal if and only if
      * both the names and values are equal.</p>
      *
      * @param o the <code>Object</code> to test for
      * equality with this <code>Pair</code>
      * @return <code>true</code> if the given <code>Object</code> is
      * equal to this <code>Pair</code> else <code>false</code>
      */
     @Override
     public boolean equals(Object o) {
         if (this == o) return true;
         if (o instanceof Pair) {
             @SuppressWarnings("rawtypes")
			Pair pair = (Pair) o;
             if (left != null ? !left.equals(pair.left) : pair.left != null) return false;
             if (right != null ? !right.equals(pair.right) : pair.right != null) return false;
             return true;
         }
         return false;
     }
     /**
      * <p>Obtains an pair of from two objects inferring the generic types.</p>
      * 
      * <p>This factory allows the pair to be created using inference to
      * obtain the generic types.</p>
      * 
      * @param <L> the left element type
      * @param <R> the right element type
      * @param left  the left element, may be null
      * @param right  the right element, may be null
      * @return a pair formed from the two parameters, not null
      */
     public static <L, R> Pair<L, R> of(L left, R right) {
    	 return new Pair<L, R>(left, right);
     }
 }


