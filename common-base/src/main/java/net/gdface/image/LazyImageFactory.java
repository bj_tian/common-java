package net.gdface.image;

import java.io.File;


/**
 * 创建 lazy image的工厂类接口
 * @author guyadong
 *
 */
public interface LazyImageFactory {
	/**
	 * 创建并打开 BaseLazyImage 实例
	 * @param imgBytes
	 * @return BaseLazyImage 实例
	 * @throws NotImageException
	 * @throws UnsupportedFormatException
	 */
	public BaseLazyImage create(final byte[] imgBytes) throws NotImageException, UnsupportedFormatException;
	/**
	 * 用本地图像文件创建 BaseLazyImage 实例
	 * @param file
	 * @param md5 {@code file}的MD5较验码，可以为null
	 * @return BaseLazyImage 实例
	 * @throws NotImageException
	 * @throws UnsupportedFormatException
	 */
	public BaseLazyImage create(final File file, String md5) throws NotImageException, UnsupportedFormatException;
	/**
	 * 通过多数据源创建 BaseLazyImage 实例
	 * @param src
	 * @return BaseLazyImage 实例
	 * @throws NotImageException
	 * @throws UnsupportedFormatException
	 */
	public <T> BaseLazyImage create(final T src) throws NotImageException, UnsupportedFormatException;
	/**
	 * 通过平台支持的图像对象创建 {@link BaseLazyImage}实例<br>
	 * @param <T> 	在标准java平台下,为 {@code java.awt.image.BufferedImage}<br>
	 * 						在android平台下为{@code android.graphics.Bitmap}<br>
	 * @param imgObj 图像对象
	 * @return BaseLazyImage 实例
	 * @throws IllegalArgumentException imageObj不是平台支持的类型
	 */
	public <T> BaseLazyImage createByImageObject(T imgObj);
}
