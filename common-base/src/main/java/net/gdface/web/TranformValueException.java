package net.gdface.web;

/**
 * 通过异常传递正常返回值
 * @author guyadong
 *
 */
public class TranformValueException extends RuntimeException {
	private static final long serialVersionUID = 3098148044189108183L;
	/**
	 * 传递的正常返回值
	 */
	private Object result;
	public TranformValueException(Object result) {
		super();
		this.result = result;
	}
	/**
	 * 正常返回值
	 */
	public Object getResult() {
		return result;
	}
}
