package net.gdface.web;

import java.util.Iterator;
import java.util.ServiceLoader;

/**
 * {@link ResponseFactory}接口默认实现
 * @author guyadong
 *
 */
public class DefaultResponseFactory implements ResponseFactory{

    @Override
    public Response createResponse() {
        return new DefaultResponse();
    }
    /**
     * SPI(Service Provider Interface)加载{@link ResponseFactory}接口实例,
     * 没有找到则返回{@link DefaultResponseFactory}实例
     * @return 返回{@link ResponseFactory}实例
     */
    public static final ResponseFactory loadResponseFactory() {
            ServiceLoader<ResponseFactory> providers = ServiceLoader.load(ResponseFactory.class);
            Iterator<ResponseFactory> itor = providers.iterator();
            return itor.hasNext() ? itor.next() : new DefaultResponseFactory();
    }  
}