package net.gdface.web;

/**
 * web响应数据接口
 * @author guyadong
 *
 */
public interface Response{
    /**
     * 接口方法调用成功
     * @param result 调用返回值
     */
    void onComplete(Object result);
    /**
     * 接口方法调用成功，调用方法返回类型为void
     */
    void onComplete();        
    /**
     * 接口方法调用抛出异常
     * @param e 异常
     */
    void onError(Exception e);
    /**
     * 返回调用结果对象
     */
    Object getResult();
    /**
     * 设置调用结果对象
     * @param result
     */
    void setResult(Object result);
    /**
     * 返回调用代理对象实例
     */
	Object delegate();
	/**
	 * 设置调用代理对象实例
	 * @param delegate
	 */
	void delegate(Object delegate);
	 /**
	 * 返回调用异常
	 */
	Exception error();
	/** 
	  * 返回调用是否成功标志 
	  */
	boolean isSuccess();
}