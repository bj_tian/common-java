package net.gdface.web;

/**
 * 获取{@link Response}接口实例的工厂类接口
 * @author guyadong
 *
 */
public interface ResponseFactory{
    /**
     * @return 返回新的{@link Response}接口实例
     */
    Response createResponse();
}